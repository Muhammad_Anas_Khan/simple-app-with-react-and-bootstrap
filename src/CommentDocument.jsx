import React from 'react';
 
const CommentDocument =(props)=>{
    return(
        
       <div className='col-12'>
            <div className="media">
                <img src = {props.image} className="mr-3 mt-2 rounded-circle" width="60px" alt="avatar"/>
                <div className="media-body">
                    <h4>{props.author} <small className="text-secondary">{props.dateAt}</small> </h4>
                    <p>{props.post}</p>
                </div>
            </div>

       </div>
    )
}

export default CommentDocument;