import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import CommentDocument from './CommentDocument';
import ApprovalCard from './ApprovalCard';

import faker from 'faker';

const App = () =>{
    return(
        <div className="container">
           <div className="row">
                <ApprovalCard>
                    <CommentDocument 
                        author='Jhone'
                        post='Nice Post.'
                        dateAt = 'Today at 4:55 PM' 
                        image={faker.image.avatar()}
                    />    
                </ApprovalCard>
                
                <ApprovalCard>
                    <CommentDocument 
                        author='Peter'
                        post='Yes this is nice book.'
                        dateAt = 'Posted on April 20, 2016' 
                        image={faker.image.avatar()}
                    />
                </ApprovalCard>
                
                <ApprovalCard>
                    <CommentDocument 
                        author='Miranda'
                        post='Good Job !!.'
                        dateAt = 'Posted on April 25, 2016' 
                        image={faker.image.avatar()}
                    />
                </ApprovalCard>
                
           </div>
        </div>
    )
}

ReactDOM.render(<App/>,document.getElementById('root'))