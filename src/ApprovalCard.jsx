import React from 'react'

const ApprovalCard =(props)=>{
    console.log(props.children)
    return(
       <div className="col-12 p-2">
        <div className="card " style={{width:'450px'}}>
            <div className="card-body">
                <div className="col-12">
                    {props.children}
                </div>
            </div>
            <div className="card-footer col-12 col-sm-12 col-md-12 bg-white">
                    <button type="button" className=" col-5 mr-1 btn btn-outline-success btn-lg">Approve</button>
                    <button type="button" className=" col-6 btn btn-outline-danger btn-lg">Reject</button>               
            </div>
        </div>
       </div>
    )
}
export default ApprovalCard;